//

console.log("Hello world!");

// Javascript can also command our browser to perform different operations just like how mathematics work
// Arithmetic operators


let x = 1397;
let y = 7831;
let sum = x+y;
console.log(sum);

let difference = x-y;
console.log(difference);

let product = x*y;
console.log(product);

let quotient = x/y;
console.log(quotient);

let remainder = y%x;
console.log(remainder);

//Assignment operator

let assignmentNumber = 8;
// assignmentNumber = assignmentNumber+2;
assignmentNumber +=2;
console.log(assignmentNumber);

assignmentNumber -=2;
console.log(assignmentNumber);

assignmentNumber *=2;
console.log(assignmentNumber);

assignmentNumber /=2;
console.log(assignmentNumber);

//this syntax will follow the mdas rule
let mdas = 1+2-3*4/5;
console.log(mdas);

// this will follow pemdas rule

let pemdas = 1+(2-3)*4/5;
console.log(pemdas);
//js can also handle more complex computations
pemdas = (1+(2-3))*(4/5);
console.log(pemdas);

//incremenet and decrement
let z=1;
let increment = ++z;
console.log("Result of pre-increment: " +increment);
console.log("Result of pre-increment: " +z);

increment = z++;
console.log("Result of post-increment: " +increment);
console.log("Result of post-increment: " +z);

let decrement =--z;
console.log("Result of pre-decrement: " +increment);
console.log("Result of pre-decrement: " +z);

decrement =z--;
console.log("Result of pre-decrement: " +increment);
console.log("Result of pre-decrement: " +z);

// Type Coercion
// is the automatic or implicit conversion of values from one data type to another
// this happends when operation are performed on different data types that would normally not be possible
// and yield irregular result
// values are automatically assigned / converted from one datatype to another in order to resolve operations

let numbA = '10';
let numbB = 12;
let coercion = numbA+numbB;
console.log(coercion);


let example = "23";
let example2 = 23;
let example3 = true;
let example4 = "true";
let example5 = false;
let example6 = 55;
let answer1 = example+example2;
let answer3 = example5*example6;
console.log(answer1);
console.log(example3/example4);
console.log(answer3);

let varA = "String plus " + true;
console.log(varA);

//Comparison Operators
//equality operator - checks wether the operands are equal/have the same content
console.log(1==1);
console.log(1==2);

console.log(1=="1");
console.log(1==true);
console.log('juan'=='juan');
let juan = 'juan';
console.log('juan'==juan);

// strict equality operator

console.log(1===1);
console.log(1===2);
console.log(1==='1');
console.log(0===false);
console.log("juan"==='juan');
console.log('juan'===juan);

// strict inquality operator
console.log(1!==1);
console.log(1!==2);
console.log(1!=='1');
console.log(0!==false);
console.log("juan"!=='juan');
console.log('juan'!==juan);


// relational operators
// some comparison operators check wether one value is greater/less than to the other value

let a = 50;
let b = 65;

let greaterThan = a>b;
let lessThan = a<b;
let greaterThanOrEqualTo = a>=b;
let lessThanOrEqualTo = a<=b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

//force coercion since it's looking for it's value since the value of the compared value is int
let numStr = "30";
console.log(a>numStr);
//string is not a numeric the string was not converted to int
let string = "twenty";
console.log(b>=string);
//NaN - not a number - result of unsuccesful conversion of string into int

//Logical Operators

let isLegalAge = true;
let isRegistered = false;

// And Operator(&&) - returns true if all values are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of And Operator: "+allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of And Operator: "+someRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Result of And Operator: "+someRequirementsNotMet);